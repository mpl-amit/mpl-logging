package com.mpl;

import android.util.Log;

import com.mpl.beautifier.BundleBeautifier;
import com.mpl.beautifier.ExceptionBeautifier;
import com.mpl.beautifier.MLogBeautifier;

import java.util.HashMap;
import java.util.Map;


public final class MLog {
    /******************************** Global Variables & Constants ********************************/
    public static final int VERBOSE = Log.VERBOSE;
    public static final int DEBUG = Log.DEBUG;
    public static final int INFO = Log.INFO;
    public static final int WARNING = Log.WARN;
    public static final int ERROR = Log.ERROR;

    public static final Object SENSITIVE = new Object();
    private static String TAG = "MLog";
    private static int LOG_LEVEL = VERBOSE;
    private static Map<Class, MLogBeautifier> BEAUTIFIERS;

    static {
        BEAUTIFIERS = new HashMap<>();
        registerBeautifier(BundleBeautifier.getInstance());
        registerBeautifier(ExceptionBeautifier.getInstance());
    }


    public static void setBaseTag(String tag) {
        TAG = tag;
    }

    public static void setLogLevel(int level) {
        LOG_LEVEL = level;
    }


    public static void registerBeautifier(MLogBeautifier beautifier) {
        BEAUTIFIERS.put(beautifier.getType(), beautifier);
    }

    public static void v(String tag, Object... params) {
        log(VERBOSE, tag, params);
    }

    private static void log(int level, String tag, Object... params) {
        if (level < LOG_LEVEL) return;
        tag = (TAG == null ? "" : TAG + "/") + tag;
        StringBuilder sb = new StringBuilder();
        boolean hideNext = false;
        for (Object o : params) {
            if (hideNext) {
                hideNext = false;
                sb.append("{XXXXXXXXXX} ");
                continue;
            }
            if (o == null) {
                sb.append("NULL ");
                continue;
            }
            if (o == SENSITIVE) {
                hideNext = true;
                continue;
            }
            if (BEAUTIFIERS.containsKey(o.getClass())) {
                try {
                    sb.append(BEAUTIFIERS.get(o.getClass()).beautify(o)).append(' ');
                } catch (Exception e) {
                    sb.append("Unhandled exception in beautifier ")
                            .append(e.getMessage()).append(' ');
                }
            } else sb.append(o).append(' ');
        }
        sb.setLength(Math.max(sb.length() - 1, 0));
        Log.println(level, tag, sb.toString());
    }

    public static void d(String tag, Object... params) {
        log(DEBUG, tag, params);
    }

    public static void i(String tag, Object... params) {
        log(INFO, tag, params);
    }

    public static void w(String tag, Object... params) {
        log(WARNING, tag, params);
    }

    public static void e(String tag, Object... params) {
        log(ERROR, tag, params);
    }
}
