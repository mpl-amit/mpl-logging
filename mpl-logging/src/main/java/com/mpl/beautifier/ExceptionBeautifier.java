package com.mpl.beautifier;

import android.util.Log;

public final class ExceptionBeautifier implements MLogBeautifier {
    private static ExceptionBeautifier INSTANCE;

    private ExceptionBeautifier() {

    }

    public static ExceptionBeautifier getInstance() {
        if (INSTANCE == null)
            INSTANCE = new ExceptionBeautifier();
        return INSTANCE;
    }

    @Override
    public Class getType() {
        return Throwable.class;
    }

    @Override
    public String beautify(Object param) {
        Throwable t = (Throwable) param;
        return String.format("\n%s\n%s\n", t.getMessage(),
                Log.getStackTraceString(t));
    }

}
