package com.mpl.beautifier;

import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

public final class BundleBeautifier implements MLogBeautifier {
    private static BundleBeautifier INSTANCE;

    private BundleBeautifier() {

    }

    public static BundleBeautifier getInstance() {
        if (INSTANCE == null)
            INSTANCE = new BundleBeautifier();
        return INSTANCE;
    }

    @Override
    public Class getType() {
        return Bundle.class;
    }

    @Override
    public String beautify(Object param) {
        Bundle b = (Bundle) param;
        JSONObject jo = new JSONObject();
        for (String key : b.keySet()) {
            try {
                jo.put(key, b.get(key));
            } catch (JSONException e) {
                try {
                    jo.put(key, "Failed to retrieve value");
                } catch (Exception ignored) {
                }
            }
        }
        return jo.toString();
    }
}
