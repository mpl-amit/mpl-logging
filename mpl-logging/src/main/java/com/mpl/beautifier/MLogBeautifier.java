package com.mpl.beautifier;

public interface MLogBeautifier {
    Class getType();

    String beautify(Object param);
}
